package com.iteyes.placesproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyData {
    @SerializedName("addresses")
    public List<MyPlace> placeList;

    public String gender;
    public String name;
    public String surname;

    public List<MyPlace> getPlaceList() {
        return placeList;
    }

    public void setPlaceList(List<MyPlace> placeList) {
        this.placeList = placeList;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
