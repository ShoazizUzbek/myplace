package com.iteyes.placesproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    AutoCompleteTextView autoCompleteTextView;
    String[]Gender_Names;
    Button login;

    Button btn_create;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        login=(Button)findViewById(R.id.login);

        btn_create=(Button)findViewById(R.id.btn_create);
        login.setOnClickListener(this);


        // autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.Gender);
        //Gender_Names = getResources().getStringArray(R.array.Gender_Names);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Gender_Names);
        //autoCompleteTextView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(StartActivity.this, PermissionsActivity.class);
        startActivity(intent);
    }




}
