package com.iteyes.placesproject.network;



import com.iteyes.placesproject.model.MyData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ApiInterFace {


    @Headers({
            "Content-Type:application/json"
    })
    @GET("patients/{number}")
    Call<MyData> loadPleaces(@Path("number")String number);

}
