package com.iteyes.placesproject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Map;

//import org.json.JSONObject;
//import org.json.simple.JSONObject;


public class UrlReader {
    URL url;
    HttpURLConnection con;

    public UrlReader(String urlString) {
        try {
            this.url = new URL(urlString);
            this.con = (HttpURLConnection) url.openConnection();
        } catch (MalformedURLException e) {
            System.out.println("mal formed url " + e);
        } catch (IOException e) {
            System.out.println("cannot open http connection " + e);
        }

    }



          /*
        JSONObject patient = new JSONObject();
     System.out.println("show post of patient 2");
        //JSONObject patient = new JSONObject();
        patient.put("patientid", "patient0002");
        patient.put("surname", "u'benali'");
        patient.put("name", "u'benomar'");
        patient.put("gender", "male");
        patient.put("phone", "[u'+213766777645', u'+21321408955']");
        JSONObject adr1 = new JSONObject();
        JSONObject adr2 = new JSONObject();
        adr1.put("number", "u'001");
        adr1.put("street", "u'larbi belmhidi'");
        adr1.put("city", "u'oran'");
        adr1.put("state", "u'algeria'");
        adr1.put("zip", "u'31200'");
        adr2.put("number", "u'045");
        adr2.put("street", "u'belabed abed'");
        adr2.put("city", "u'algiers'");
        adr2.put("state", "u'algeria'");
        adr2.put("zip", "u'16100'");
        patient.put("addresses","["+adr1+","+adr2+"]" );
      //  element = patient.toString();
         element =patient.toJSONString();

      //  element = patien
        ur = new UrlReader(urlString);
       // ur.postElement(element);
        System.out.println(ur.postElement(element));
        //y'a un soucis ici avec le postElement
 */

   /*     System.out.println("show put of patient 2");
        patient = new JSONObject();
        patient.put("surname", "u'omar'");
        //  element =patient.toJSONString();
        element = patient.toString();
        ur = new UrlReader(urlString + "/1");
        content = ur.putElement(element);
        System.out.println(content);*/

    /*    System.out.println("show delete of task 3");
        element ="/3";
        ur = new UrlReader(urlString+element);
        content = ur.deleteElement();
        System.out.println(content);*/




    public StringBuffer getElement() throws ProtocolException, IOException {
        BufferedReader in;
        String inputLine;
        StringBuffer content = new StringBuffer();
        this.con.setRequestProperty("Content-Type", "application/json");
        this.con.setDoInput(true);
        this.con.setUseCaches(false);
        this.con.setRequestMethod("GET");

        in = new BufferedReader(new InputStreamReader(this.con.getInputStream()));
        while ((inputLine = in.readLine()) != null) content.append(inputLine);
        in.close();
        return content;
    }


    public StringBuffer postElement(String element) throws ProtocolException, IOException {

        BufferedReader in;
        String inputLine;
        StringBuffer content = new StringBuffer();
        this.con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        this.con.setDoOutput(true);
        this.con.setDoInput(true);
        this.con.setUseCaches(false);
        this.con.setRequestMethod("POST");

        OutputStream os = this.con.getOutputStream();
        os.write(element.getBytes("UTF-8"));
        os.close();

        // read the response
        in = new BufferedReader(new InputStreamReader(this.con.getInputStream()));
        while ((inputLine = in.readLine()) != null) content.append(inputLine);
        in.close();
        return content;


    }  // end of postNewItem() method

    public StringBuffer putElement(String element) throws ProtocolException, IOException {
        BufferedReader in;
        String inputLine;
        StringBuffer content = new StringBuffer();
        this.con.setRequestProperty("Content-Type", "application/json");
        this.con.setDoOutput(true);
        this.con.setDoInput(true);
        this.con.setUseCaches(false);
        this.con.setRequestMethod("PUT");

        OutputStream os = this.con.getOutputStream();
        os.write(element.getBytes("UTF-8"));
        os.close();

        in = new BufferedReader(new InputStreamReader(this.con.getInputStream()));
        while ((inputLine = in.readLine()) != null) content.append(inputLine);
        in.close();
        return content;
    }

    public StringBuffer deleteElement() throws ProtocolException, IOException {
        BufferedReader in;
        String inputLine;
        StringBuffer content = new StringBuffer();
        this.con.setRequestProperty("Content-Type", "application/json");
        this.con.setDoInput(true);
        this.con.setUseCaches(false);
        this.con.setRequestMethod("DELETE");
        in = new BufferedReader(new InputStreamReader(this.con.getInputStream()));
        while ((inputLine = in.readLine()) != null) content.append(inputLine);
        in.close();
        return content;
    }  // end of postNewItem() method


}

